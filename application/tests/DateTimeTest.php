
<?php
use PHPUnit\Framework\TestCase;
class DateTimeTest extends TestCase
{
/**
* @test
*/
public function shouldFormatDate() {
$dateTime = new DateTime('2016-09-01');
$this->assertEquals('01/09/2016', $dateTime->format('d/m/Y'));
}
}